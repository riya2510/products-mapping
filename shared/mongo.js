const mongoose = require('mongoose');
mongoose.Promise = global.Promise;

class Mongo {
    constructor() {
        const mongoConfig = global.CONFIG['mongo'];
        this.uri = 'mongodb://';

        if (mongoConfig['username'] !== '' && mongoConfig['password'] !== '') {
            this.uri += `${mongoConfig['username']}:${encodeURIComponent(mongoConfig['password'])}@`;
        }

        this.uri += `${mongoConfig['hosts'].join(',')}/${mongoConfig['database']}`;

        if (mongoConfig['replicaSet'] !== '') {
            this.uri += `?replicaSet=${mongoConfig['replicaSet']}&readPreference=primaryPreferred`;
        }

        const options = {
            autoIndex: false, // Don't build indexes
            reconnectTries: Number.MAX_VALUE, // Never stop trying to reconnect
            reconnectInterval: 500, // Reconnect every 500ms
            poolSize: 10, // Maintain up to 10 socket connections
            // If not connected, return errors immediately rather than waiting for reconnect
            bufferMaxEntries: 0,
            useNewUrlParser: true,
            useFindAndModify: false,
            replicaSet: mongoConfig['replicaSet'],
            authSource: 'admin',
            useUnifiedTopology: true,
        };

        mongoose
            .connect(
                this.uri,
                options,
            )
            .then(
                () => {
                    console.info(`Worker ${process.pid} connected to Mongo Database`);
                    global.Mongoose = mongoose;
                },
                err => {
                    console.error(`Worker ${process.pid} failed connecting to Mongo Database: ${err}`);
                },
            );
    }
}

module.exports = Mongo;
