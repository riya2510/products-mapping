const fs = require('fs');
const request = require('request');
const https = require('https');
const readline = require('readline');

const { promisify } = require('util');
const mkDir = promisify(fs.mkdir);
const exists = promisify(fs.exists);

const ENV = process.env.NODE_ENV || 'local';
const configFile = `./config/${ENV}.json`;
const config = require(configFile);
global.CONFIG = config;
const type = process.argv[2];
const startingPoint = process.argv[3] ? parseInt(process.argv[3]) : 0;

const Mongo = require('./shared/mongo');
new Mongo();

const CategoryModel = require('./models/category.model');
const DepartmentModel = require('./models/department.model');
const DepartmentGroupModel = require('./models/department-group.model');
const ProductModel = require('./models/product.model');
const FilesModel = require('./models/files.model');

const imagePath = `${__dirname}/images`;
let imageURIs = {};

const foldersData = {
  'root': {
    _id: '5bd6ab66468f9c0cd64a6b31'
  },
  'store-icons': {
    _id: '5bdb0350863b8e252799004d'
  },
  'categories': {
    _id: '5f33cafcfe7f20be4f0144cf'
  },
  'products': {
    _id: '5bd6fb216529c35b4a4834e7'
  }
}

class ProductsDataMapping {
  static async execute() {
    switch (type) {
      case 'departmentGroup':
        await departmentGroupProcess();
        break;
      case 'department':
        await departmentProcess();
        break;
      case 'category':
        await categoryProcess();
        break;
      case 'product':
        await productProcess();
        break;
    }
  }
}

async function departmentGroupProcess() {
  const departmentGroupsData = await new DepartmentGroupModel().departmentGroup.find({},
    { Image150: 1, Image300: 1, Image600: 1, Image800: 1, Image1024: 1}).lean().exec();

  if(departmentGroupsData && departmentGroupsData.length) {
    const keepAliveAgent = new https.Agent({ keepAlive: true, maxSockets: 20, maxFreeSockets: 10 });
    await collectImageURIs(departmentGroupsData);
    await downloadImages(`${imagePath}/department-group`, 'store-icons', new DepartmentGroupModel().departmentGroup, keepAliveAgent);
  }
  imageURIs = {};
  console.info(`Depatment-Group Images downloaded`);
}

async function departmentProcess() {
  const departmentData = await new DepartmentModel().freshFoodDepartment.find({},
    { Image150: 1, Image300: 1, Image600: 1, Image800: 1, Image1024: 1}).lean().exec();

  if(departmentData && departmentData.length) {
    const keepAliveAgent = new https.Agent({ keepAlive: true, maxSockets: 20, maxFreeSockets: 10 });
    await collectImageURIs(departmentData);
    await downloadImages(`${imagePath}/department`, 'root', new DepartmentModel().freshFoodDepartment, keepAliveAgent);
  }
  imageURIs = {};
  console.info(`Depatment Images downloaded`);
}

async function categoryProcess() {
  const categoryData = await new CategoryModel().category.find({},
    { Image150: 1, Image300: 1, Image600: 1, Image800: 1, Image1024: 1}).lean().exec();

  if(categoryData && categoryData.length) {
    const keepAliveAgent = new https.Agent({ keepAlive: true, maxSockets: 20, maxFreeSockets: 10 });
    await collectImageURIs(categoryData);
    await downloadImages(`${imagePath}/category`, 'categories', new CategoryModel().category, keepAliveAgent);
  }
  imageURIs = {};
  console.info(`Category Images downloaded`);
}

async function productProcess() {
  const productData = await new ProductModel().product.find({},
    { Image150: 1, Image300: 1, Image600: 1, Image800: 1, Image1024: 1}).lean().exec();

  if(productData && productData.length) {
    const keepAliveAgent = new https.Agent({ keepAlive: true, maxSockets: 20, maxFreeSockets: 10 });
    await collectImageURIs(productData);
    await downloadImages(`${imagePath}/product`, 'products', new ProductModel().product, keepAliveAgent);
  }
  imageURIs = {};
  console.info(`Product Images downloaded`);
}

async function collectImageURIs(data) {
  for(let i = 0; i < data.length; i++) {
    if(data[i].Image150 && data[i].Image150.includes('rackcdn')) {
      imageURIs[`${data[i]._id}_150`] = data[i].Image150;
    }
    if(data[i].Image300 && data[i].Image300.includes('rackcdn')) {
      imageURIs[`${data[i]._id}_300`] = data[i].Image300;
    }
    if(data[i].Image600 && data[i].Image600.includes('rackcdn')) {
      imageURIs[`${data[i]._id}_600`] = data[i].Image600;
    }
    if(data[i].Image800 && data[i].Image800.includes('rackcdn')) {
      imageURIs[`${data[i]._id}_800`] = data[i].Image800;
    }
    if(data[i].Image1024 && data[i].Image1024.includes('rackcdn')) {
      imageURIs[`${data[i]._id}_1024`] = data[i].Image1024;
    }
  }
}

async function downloadImages(path, prefix, model, agent) {
  const images = Object.entries(imageURIs).filter(Boolean);
  let extension = '';
  for (let i = startingPoint; i < images.length; i++) {
    const data = images[i];
    try {
      await new Promise((resolve) => {
        try {
          request.head(data[1], (err, res) => {
            if(res && res.statusCode === 200) {
              extension = ((res.headers['content-type']).slice(res.headers['content-type'].lastIndexOf('/') + 1)).toLowerCase();
            } else {
              const ext = ((data[1]).slice(data[1].lastIndexOf('.') + 1)).toLowerCase();
              extension = ['png', 'jpg', 'jpeg', 'jfif'].includes(ext) ? ext : 'jfif';
            }
          });

          request.get(data[1], { headers: {
            'User-Agent': agent,
            'Connection': 'keep-alive',
            "Keep-Alive": "max=5000"
          } })
            .pipe(fs.createWriteStream(`${path}/${data[0]}.${extension}`))
            .on('error', (error) => console.info('error occured', error))
            .once('close', () => resolve(`${path}/${data[0]}.${extension}`));
        } catch(error) {
          console.error('error from downloadimage');
          console.error(error);
        }
      });
      await updateCollectionData(data[0], extension, prefix, model);
      const progress = parseInt((i / images.length * 100).toString());
      await printProgress(progress, `Downloading ${type} Images ${i + 1} of ${images.length}`);
    } catch(error) {
      console.error(error);
    }
  }
}

async function updateCollectionData(imageName, extension, prefix, model, updateObj = {}) {
  const [docId, size] = imageName.split('_');
  updateObj[`Image${size}`] = `${prefix}/${imageName}.${extension}`;
  await model.findByIdAndUpdate(docId, updateObj).lean().exec();
  updateObj['ImageName'] = `${docId}.${extension}`;
  updateObj['FolderId'] = `${foldersData[prefix]._id}`;
  updateObj['FileName'] = `${docId}.${extension}`;
  await new FilesModel().files.findOneAndUpdate({ ImageName: `${docId}.${extension}` }, updateObj, { upsert: true });
}

function printProgress(progress, custom) {
  readline.cursorTo(process.stdout, 0);
  process.stdout.write(`${custom} ${'#'.repeat(progress)}${'.'.repeat(100 - progress)} ${progress}%`);
}

function sleep(ms) {
  return new Promise(resolve => {
    setTimeout(resolve, ms);
  })
}

return new Promise(async () => {
  try {
    await sleep(3000);
    if(!await exists(`${imagePath}/department-group`)) {
      await mkDir(`${imagePath}/department-group`);
    }
    if(!await exists(`${imagePath}/department`)) {
      await mkDir(`${imagePath}/department`);
    }
    if(!await exists(`${imagePath}/category`)) {
      await mkDir(`${imagePath}/category`);
    }
    if(!await exists(`${imagePath}/product`)) {
      await mkDir(`${imagePath}/product`);
    }
    await ProductsDataMapping.execute();
  } catch (error) {
    console.error(`Error ${error}`);
  } finally {
    process.exit(0);
  }
});
