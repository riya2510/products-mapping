class DepartmentModel {
    constructor() {
        try {
            this.freshFoodDepartment = global.Mongoose.model('FreshFoodDepartment');
        } catch (error) {
            this.__freshFoodDepartmentSchema = new global.Mongoose.Schema({
                DepartmentName: {type: String},
                Active: {type: Boolean},
                NotAvailableOnStores: {type: Array},
                DepartmentGroup: {type: String},
                Quantity: {type: Boolean},
                Weight: {type: Boolean},
                Thickness: {type: Boolean},
                KPSNotifications: {type: Array}, // ex: [{'ColorName': 'green', 'ColorCode': '#008000', 'Time':10, 'TimeOption':1}] 1 for minute & 2 for hour
                DepartmentDesc: {type: String},
                OpensAt: {type: String},
                ClosesAt: {type: String},
                LeadTime: {type: Number},
                CutOffTime: {type: String},
                LeadTimeOption: {type: Number}, // 1 for minute & 2 for hour
                SlotIncrementTime: {type: Number},
                IncrementTimeOption: {type: Number}, // 1 for minute & 2 for hour
                FirstTimeslot: {type: String},
                LastTimeslot: {type: String},
                MaxOrderAllow: {type: Number},
                ImageName: {type: String},
                Image150: {type: String},
                Image300: {type: String},
                Image600: {type: String},
                Image800: {type: String},
                Image1024: {type: String},
                CreatedDate: {type: Number},
                ModifiedDate: {type: Number},
                IsDeleted: {type: Boolean, default: false},
                Private: {type: Boolean, default: false},
                SortOrder: {type: Number},
                QtyOfTimeSlots: {type: Number},
            }, {versionKey: false});
            this.freshFoodDepartment = global.Mongoose.model('FreshFoodDepartment', this.__freshFoodDepartmentSchema);
        }
    }
}
module.exports = DepartmentModel;
