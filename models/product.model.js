class ProductModel {
    constructor() {
        try {
            this.product = global.Mongoose.model('FreshFoodProduct');
        } catch (error) {
            this.__availableOnStoreSchema = new global.Mongoose.Schema({
                StoreNumber: {type: String},
                Price: {type: String},
                SellPrice: {type: String, default: ''},
                Tax: {type: String}, //EA996 Amit Pandya
            }, {versionKey: false});

            this.__activationScheduleSchema = new global.Mongoose.Schema({
                StartDate: {type: Number},
                EndDate: {type: Number},
            }, {versionKey: false}, {_id: false});

            this.__ingredientsDetailsSchema = new global.Mongoose.Schema({
                IngredientGroupId: {type: String},
                IsRequired: {type: Boolean},
                SelectedItem: {type: String, default: ''},
            }, {versionKey: false});

            this.__freshFoodProductSchema = new global.Mongoose.Schema({
                ProductName: {type: String},
                ProductCode: {type: String},
                DepartmentId: {type: String},
                CategoryId: {type: String},
                Ingredients: [this.__ingredientsDetailsSchema],
                Active: {type: Boolean},
                ShortDesc: {type: String},
                LongDesc: {type: String},
                ServingSize: {type: String},
                Price: {type: String},
                PriceUnit: {type: String},
                ItemImage: {type: String},
                Image150: {type: String},
                Image300: {type: String},
                Image600: {type: String},
                Image800: {type: String},
                Image1024: {type: String},
                NotAvailableOnStores: {type: Array},
                AvailableOnStores: [this.__availableOnStoreSchema],
                CreatedBy: {type: String, ref: 'User'},
                CreatedDate: {type: Number},
                EPCCode: {type: String},
                NutritionNum: {type: Number},
                NutritionOnOwnLabel: {type: Boolean},
                OutOfStockOnStores: {type: Array},	//stores reference
                IsPrivate: {type: Boolean, default: false},
                IsDeleted: {type: Boolean, default: false},
                IsFlexible: {type: Boolean, default: false},
                SortOrder: {type: Number},
                Calories: {type: String, default: ''},
                IsFeaturedItem: {type: Boolean, default: false},
                ActiveOn: [this.__activationScheduleSchema],
                IsQuantity: {type: Boolean},
                IsWeight: {type: Boolean},
                IsThickness: {type: Boolean},
            }, {versionKey: false});
            this.product = global.Mongoose.model('FreshFoodProduct', this.__freshFoodProductSchema);
        }
    }
}
module.exports = ProductModel;
