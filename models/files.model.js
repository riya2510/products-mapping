class FilesModel {
    constructor() {
        try {
            this.files = global.Mongoose.model('Files');
        } catch (error) {
            this.__filesSchema = new global.Mongoose.Schema({
                ImageName: { type: String },
                Image150: { type: String },
                Image300: { type: String },
                Image600: { type: String },
                Image800: { type: String },
                Image1024: { type: String },
                FolderId: { type: String },
                FileName: { type: String },
            }, {
                collection: 'files',
                versionKey: false
            });
            this.files = global.Mongoose.model('Files', this.__filesSchema);
        }
    }
}
module.exports = FilesModel;
