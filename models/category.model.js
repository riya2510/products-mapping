class CategoryModel {
    constructor() {
        try {
            this.category = global.Mongoose.model('FreshFoodCategory');
        } catch (error) {
            this.__FreshFoodCategorySchema = new global.Mongoose.Schema({
                CategoryName: {type: String},
                ParentCategoryId: {type: String},
                HasChild: {type: Boolean},
                NotAvailableOnStores: {type: Array},
                Active: {type: Boolean},
                DepartmentId: {type: String},
                ImageName: {type: String},
                Image150: {type: String},
                Image300: {type: String},
                Image600: {type: String},
                Image800: {type: String},
                Image1024: {type: String},
                Description: {type: String},
                IsDeleted: {type: Boolean, default: false},
                Private: {type: Boolean, default: false},
                SortOrder: {type: Number},
            }, {versionKey: false});
            this.category = global.Mongoose.model('FreshFoodCategory', this.__FreshFoodCategorySchema);
        }
    }
}
module.exports = CategoryModel;
