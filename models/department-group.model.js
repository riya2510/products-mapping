class DepartmentGroup {
    constructor() {
        try {
            this.departmentGroup = global.Mongoose.model('FreshFoodDepartmentGroup');
        } catch (error) {
            this.__availableOnStoreSchema = new global.Mongoose.Schema({
                StoreNumber: { type: String },
                DeliveryFee: { type: String, default: 0 },
            }, { versionKey: false });

            this.__departmentGroupSchema = new global.Mongoose.Schema({
                GroupName: { type: String, unique: true },
                AdvanceOrdering: { type: String },
                GroupImage: { type: String },
                ShortDesc: { type: String },
                LongDesc: { type: String },
                Image150: { type: String },
                Image300: { type: String },
                Image600: { type: String },
                Image800: { type: String },
                Image1024: { type: String },
                IsDeleted: { type: Boolean, default: false },
                VariableDays: { type: Number, default: 0 },
                AllowSameDay: { type: Boolean, default: false },
                Private: { type: Boolean, default: false },
                SortOrder: { type: Number },
                AvailableInStores: [this.__availableOnStoreSchema], //EA-893 Code Develop by Amit Pandya,EA-995
                Active: { type: Boolean, default: true }, //EA-893 Code Develop by Amit Pandya
                Delivery: { type: Boolean, default: false }, //EA-995 Amit Pandya
                PaymentRequired: { type: Boolean, default: false }, //EA-995 Amit Pandya
                SectionId: { type: String, ref: 'Section' }, //EA-994 Amit Pandya
            }, { versionKey: false });
            this.departmentGroup = global.Mongoose.model('FreshFoodDepartmentGroup', this.__departmentGroupSchema);
        }

    }
}

module.exports = DepartmentGroup;
